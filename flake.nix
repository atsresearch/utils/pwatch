{
  description = "pwatch -- process monitor";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, flake-utils, ... }@inputs:


    flake-utils.lib.eachDefaultSystem (system: let

      overlay = super: self: {

        python = super.python310;

        procps_override = self.procps.overrideAttrs (oldAttrs: rec {

          # toprc may not work unless the version is exactly 3.3.17
          version = "3.3.17";

          src = self.fetchurl {
            url = "mirror://sourceforge/procps-ng/procps-ng-${version}.tar.xz";
            sha256 = "RRiz56r9NOwH0AY9JQ/UdJmbILIAIYw65W9dIRPxQbQ=";
          };

          patches = null;

        });

      };

      pkgs = import inputs.nixpkgs { 
        inherit system;
        overlays = [ overlay ];
      };
      pythonPackages = pkgs.python.pkgs;

    in {

      defaultPackage = pythonPackages.buildPythonPackage {
        name = "pwatch";
        src = ./.;
        propagatedBuildInputs = with pythonPackages; [
          pkgs.procps_override
          ipdb
          ipython
        ];
        doCheck = false;
        postInstall = ''

          top_apdn=$out/share/top
          mkdir -p $top_apdn
          toprc_apfn=$top_apdn/toprc
          cp config/toprc $toprc_apfn

          wrapProgram $out/bin/pwatch \
            --set TOPRC_PATH $toprc_apfn

        '';
        postShellHook = ''

          PATH="${pkgs.procps_override}/bin''${PATH:+:$PATH}"

          prepend_pythonpath_bin_dirs_to_path() {

            local -a arr

            IFS=: read -a arr <<< "$PYTHONPATH"
            local x
            local bin_apdn
            for x in "''${arr[@]}"; do
              bin_apdn=$x/../../../bin
              [[ -d $bin_apdn ]] && PATH=$bin_apdn''${PATH:+:$PATH}
            done

          }
          prepend_pythonpath_bin_dirs_to_path

        '';
      };

      defaultApp = {
        type = "app";
        program = "${self.defaultPackage.${system}}/bin/pwatch";
      };

    });
}
