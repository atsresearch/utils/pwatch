from setuptools import setup



setup(name="pwatch",
      version="0.0.1",
      packages=["pwatch"],
      package_dir={"pwatch": "src"},
      entry_points={
          "console_scripts": [ "pwatch = pwatch.pwatch:main" ]
      }
)

