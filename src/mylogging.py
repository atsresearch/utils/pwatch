import logging
import logging.handlers
import sys
import re
import multiprocessing

from time import gmtime
from functools import (
    partial,
    partialmethod,
)

from typing import (
    Callable,
    Union,
    Optional,
    Any,
)


class UpperCapFilter(logging.Filter):
    """Filter used to pick only records that have levelno below cutofflevel."""

    def __init__(
        self,
        cutofflevel,
    ) -> None:
        
        self.cutofflevel = cutofflevel

    #TODO type hints
    def filter(
        self,
        record,
    ):
        
        return record.levelno < self.cutofflevel


def disable_logger(name: str) -> None:
    logger = logging.getLogger(name)
    logger.disabled = True
    logger.propagate = False


def add_custom_log_level(name: str, severity: int) -> None:

    """Add a custom log level.

    If level with same severity value has already been added does nothing.
    """

    if not re.fullmatch(r"[A-Z_]+", name):
        raise ValueError(f'Invalid value `{name=}`')

    if logging.getLevelName(severity) == name:
        # already added, further additions are no-op
        return

    if hasattr(logging, name):
        raise ValueError(f'Invalid value `{name=}` as it is already '
                          'defined as a logging module level variable')

    name_lower = name.lower()

    if hasattr(logging, name):
        raise ValueError(f'Invalid value `{name=}` as `{name_lower}` is '
                          'already defined as a logging module level variable')

    Logger = logging.Logger
    if hasattr(Logger, name_lower):
        raise ValueError(f'Invalid value `{name=}` as `{name_lower}` '
                          'already exists in logging.Logger')

    setattr(logging, name, severity)
    logging.addLevelName(severity, name)
    setattr(Logger, name_lower, partialmethod(Logger.log, severity))
    setattr(logging, name_lower, partial(logging.log, severity))


def add_std_custom_log_levels() -> None:
    add_custom_log_level("TRACE", 5)
    add_custom_log_level("TRACE_EXT", 6)
    add_custom_log_level("TRACE_INT", 7)


def utc_formatter(*args, **kwargs) -> logging.Formatter:
    formatter = logging.Formatter(*args, **kwargs)
    formatter.converter = gmtime
    return formatter


def setup_root_logger(
    log_level,
    **kwargs,
) -> None:

    add_std_custom_log_levels()

    fmt = kwargs.pop(
        "fmt",
        "%(asctime)s.%(msecs)03d [%(levelname)s] %(message)s",
    )
    datefmt = kwargs.pop(
        "datefmt",
        "%H:%M:%S",
    )
    # if given, `fmt` and `datefmt` are no-op
    formatter = kwargs.pop(
        "formatter",
        utc_formatter(fmt=fmt, datefmt=datefmt),
    )
    if kwargs:
        raise ValueError(f'Unexpected `kwargs`: `{sorted(kwargs.keys())}`')

    logger = logging.root
    logger.setLevel(log_level)
    logger.handlers.clear()

    stdout = logging.StreamHandler(stream=sys.stdout)
    stdout.name = "stdout"
    stdout.addFilter(UpperCapFilter(logging.WARNING))
    stdout.setFormatter(formatter)
    logger.addHandler(stdout)

    stderr = logging.StreamHandler(stream=sys.stderr)
    stderr.name = "stderr"
    stderr.setLevel(logging.WARNING)
    stderr.setFormatter(formatter)
    logger.addHandler(stderr)


def monkey_patch_handler_for_mp() -> None:
    def patched_createLock(self):
        self.lock = multiprocessing.RLock()
    logging.Handler.createLock = patched_createLock


def setup_basic_logging(
    log_level,
    **kwargs,
) -> None:
    
    # parso loggers are annoying when using ipdb
    disable_logger("parso")
    return setup_root_logger(log_level, **kwargs)

        
def printing_logrecord_handler(
    logrecord: logging.LogRecord,
    *args,
    **kwargs,
) -> None:

    print(logrecord.getMessage())
