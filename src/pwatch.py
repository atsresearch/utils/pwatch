import subprocess
import logging
import asyncio
import argparse
import json
import os
import re
import sys
import shutil
import atexit
# import pandas as pd
from time import time
# from pandas import DataFrame, Series, Timestamp
from pprint import pprint, pformat
from contextlib import suppress
from tempfile import TemporaryDirectory
from copy import copy, deepcopy
from .mylogging import setup_basic_logging, disable_logger



###############################################################################


SCRIPT_APDN = os.path.dirname(os.path.realpath(__file__))
ORIG_PWD = os.environ["PWD"]


###############################################################################


L = logging.root

class GlobalState:
    pass
g = GlobalState()



###############################################################################


async def prun(
        executable,
        p_args=None,
        env=None,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE):
    if not p_args:
        p_args = []
    p = await asyncio.create_subprocess_exec(
        executable,
        *p_args,
        stdout=stdout,
        stderr=stderr,
        env=env,
    )
    stdout, stderr = await p.communicate()
    stdout = stdout.decode() if stdout else ""
    stderr = stderr.decode() if stderr else ""
    # L.info(f'{len(stdout)=} {len(stderr)=}')
    try:
        await asyncio.wait_for(p.wait(), 1)
    except asyncio.TimeoutError:
        raise
    # stderr = (await p.stderr.read()).decode()
    if p.returncode != 0:
        raise RuntimeError(f'error executing subprocess \'{executable}\':\n\n'
                           f'args: {pformat(p_args)}\n\n'
                           f'stderr:\n{stderr}')
    # stdout = (await p.stdout.read()).decode()
    return stdout


def parse_tabular_output(lines, sep=" "):
    header = lines[0]
    cols = header.split()
    res = []
    for line in lines[1:]:
        d = {}
        cells = line.split()
        for col, cell in zip(cols, cells):
            d[col] = cell
        res.append(d)
    return res


async def fetch_ps():
    p_args = [
        "-e",
        "H",
        "-o",
        "pid,ppid,spid,lwp,nlwp,etimes,times,pmem,drs,rss,size,vsize,sz,trs,comm",
    ]
    res = await prun("ps", p_args)
    lines = res.splitlines()
    return parse_tabular_output(lines)


async def fetch_top():
    p_args = [
        "-b",
        "-H",
        "-n",
        "1",
    ]
    env = {
        "HOME": g.temp_dir.name,
        "TERM": "xterm",
        "PATH": os.environ["PATH"],
    }
    # if stderr is pipe procps top does not use $HOME/.toprc for some reason
    res = await prun("top", p_args=p_args, env=env, stderr=None)
    res = re.search(r"(\s+PID.*)", res, flags=re.DOTALL).group(1)
    lines = [x for x in res.splitlines() if x.strip()]
    return parse_tabular_output(lines)


def parse_top_bytes(x):
    if "g" in x:
        # not sure if 1024 or 1000 base is used here
        mult = 1024 ** 3
        x = float(x[:-1])
    else:
        mult = 1
        x = int(x)
    return round(x * mult)


def collect_data(ps, top, curr_pid=None):

    if curr_pid is None:
        curr_pid = g.args.pid

    res = {}

    proc_apdn = f'/proc/{curr_pid}'
    with suppress(Exception):
        res["num_fds"] = len(os.listdir(f'{proc_apdn}/fd'))
        # with open(f'{proc_apdn}/cmdline') as f:
        #     res["argv"] = f.read().split("\0")

    matches = [x for x in ps if x["PID"] == str(curr_pid)]
    if not matches:
        raise RuntimeError(f'process {g.args.pid} not found')

    root = [x for x in matches if x["PID"] == x["SPID"]][0]

    # sanity check ps output
    assert int(root["NLWP"]) == len(matches)

    res["ts"] = time()
    res["pid"] = int(root["PID"])
    res["drs"] = int(root["DRS"])
    res["rss"] = int(root["RSS"])
    res["size"] = int(root["SIZE"])
    res["vsize"] = int(root["VSZ"])
    res["sz"] = int(root["SZ"])
    res["trs"] = int(root["TRS"])
    res["pmem"] = float(root["%MEM"]) / 100
    if root["ELAPSED"] != "0":
        res["pcpu"] = float(root["TIME"]) / float(root["ELAPSED"])
    else:
        res["pcpu"] = 0
    res["times"] = int(root["TIME"])
    res["etimes"] = int(root["ELAPSED"])
    res["comm"] = root["COMMAND"]
    res["lwps"] = lwps = {}
    for lwp in matches:
        d = {}
        d["etimes"] = int(lwp["ELAPSED"])
        d["times"] = int(lwp["TIME"])
        d["comm"] = lwp["COMMAND"]
        lwps[int(lwp["SPID"])] = d

    matches = [x for x in top if x["TGID"] == str(curr_pid)]
    if matches:
        troot = [x for x in matches if x["PID"] == x["TGID"]]
        if len(troot) != 1:
            # On some processes there is no thread with "PID" ...
            # This happens when process group leader created the thread
            # or something ... not clear.
            troot = matches[0]
        else:
            troot = troot[0]
        res["top_pcpu"] = float(troot["%CPU"]) / 100
        res["top_pmem"] = float(troot["%MEM"]) / 100
        res["top_code"] = int(troot["CODE"])
        res["top_data"] = int(troot["DATA"])
        res["top_ooma"] = int(troot["OOMa"])
        res["top_ooms"] = int(troot["OOMs"])
        res["top_res"] = troot["RES"]
        res["top_rsan"] = troot["RSan"]
        res["top_status"] = troot["S"]
        res["top_shr"] = int(troot["SHR"])
        res["top_swap"] = parse_top_bytes(troot["SWAP"])
        res["top_used"] = parse_top_bytes(troot["USED"])
        res["top_virt"] = parse_top_bytes(troot["VIRT"])
        res["top_ndrt"] = int(troot["nDRT"])
        res["top_nmaj"] = troot["nMaj"]
        res["top_nmin"] = troot["nMin"]
        res["top_vmj"] = troot["vMj"]
        res["top_vmn"] = troot["vMn"]
        for lwp in matches:
            d = lwps.get(int(lwp["PID"]))
            if not d:
                continue
            d["top_pcpu"] = float(lwp["%CPU"]) / 100
            d["top_nmaj"] = lwp["nMaj"]
            d["top_nmin"] = lwp["nMin"]

    res["children"] = []
    children = [x for x in ps
                if x["PPID"] == root["PID"] and x["PID"] == x["SPID"]]
    for child in children:
        r = collect_data(ps, top, curr_pid=int(child["PID"]))
        res["children"].append(r)

    return res


async def fetch_all():

    coros = []

    coros.append(fetch_ps())
    coros.append(fetch_top())

    res = await asyncio.gather(*coros)
    ps = res[0]
    top = res[1]

    res = collect_data(ps, top)

    # print(DataFrame(matches))

    # children = [x for x in ps
    #             if x["SPID"] == root["PID"] and x["PID"] != root["PID"]]
    # L.info(pformat(root))
    # L.info(pformat(children))

    # res["ps"] = r[0]

    return res


###############################################################################



def parse_args():
    desc = "ptrace -- process tracking utility"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("pid", type=int, help="process (pid) to track")
    parser.add_argument(
        "-n", "--interval",
        type=float,
        default=1.0,
        help="measurement interval"
    )
    parser.add_argument(
        "--rss",
        action="store_true",
        help="show rss instead of %%mem"
    )
    parser.add_argument("-o", "--output", help="output file")
    parser.add_argument("--log-level", default="INFO", help="logging level")
    args = parser.parse_args()
    return args


def setup_logging():
    # fmt = "%(asctime)s.%(msecs)03d [%(name)s] [%(levelname)s] %(message)s"
    fmt = "%(asctime)s.%(msecs)03d [%(levelname)s] %(message)s"
    setup_basic_logging(g.args.log_level, fmt=fmt)
    disable_logger("asyncio")


def extract_recursively(data):
    res = {
        "top_pcpu": data.get("top_pcpu", 0),
        "pmem": data["pmem"],
        "rss": data["rss"],
        "lwps": len(data["lwps"]),
        "num_fds": data.get("num_fds", 0),
        "count": 1,
    }
    for child in data["children"]:
        r = extract_recursively(child)
        for k in r.keys():
            res[k] += r[k]
    return res


def compress_to_line(data):
    rdata = extract_recursively(data)
    cols = []
    cols.append(f'PID: {data["pid"]}')
    pcpu_s = f'{data["top_pcpu"]:.2%}' if "top_pcpu" in data else "?"
    cols.append(f'CPU: {pcpu_s}/{rdata["top_pcpu"]:.2%}')
    if g.args.rss:
        cols.append(f'RSS: {data["rss"]:}/{rdata["rss"]:}')
    else:
        cols.append(f'MEM: {data["pmem"]:.2%}/{rdata["pmem"]:.2%}')
    cols.append(f'#P: {rdata["count"]}')
    cols.append(f'#LWP: {len(data["lwps"])}/{rdata["lwps"]}')
    cols.append(f'#FD: {data["num_fds"]}/{rdata["num_fds"]}')
    s = ", ".join(cols)
    return s


def at_exit():
    # os.chdir(ORIG_PWD)
    if g.temp_dir:
        L.debug(f'cleaning up temporary directory {g.temp_dir.name} ...')
        g.temp_dir.cleanup()
        L.debug(f'temporary directory {g.temp_dir.name} cleaned up')


def setup_virtual_home():
    toprc_xpfn = os.environ.get(
        "TOPRC_PATH",
        os.path.join(SCRIPT_APDN, "..", "config", "toprc")
    )
    shutil.copy(f'{toprc_xpfn}', f'{g.temp_dir.name}/.toprc')
    os.environ["HOME"] = g.temp_dir.name


async def async_main():
    
    while True:

        res = await fetch_all()

        if g.args.output:
            with open(g.args.output, "a") as f:
                f.write(f'{json.dumps(res)}\n')

        L.info(compress_to_line(res))

        await asyncio.sleep(g.args.interval)


def main():

    g.args = parse_args()

    setup_logging()

    g.temp_dir = TemporaryDirectory(prefix="pwatch_",
                                    ignore_cleanup_errors=False)
    # os.chdir(g.temp_dir.name)

    atexit.register(at_exit)

    setup_virtual_home()

    try:
        asyncio.run(async_main())
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()




